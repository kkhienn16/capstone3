import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom'; 
import { Container, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import App from '../App.css';

export default function Login() {

	const { user, setUser } = useContext(UserContext);

	
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);


    function authenticate(e) {

 
        e.preventDefault();
		fetch('https://capstone2-1.onrender.com/users/login',{

		method: 'POST',
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({

			email: email,
			password: password

		})
	})
	.then(res => res.json())
	.then(data => {
		
		if(data.access){

			// Set the email of the authenticated user in the local storage
		    // Syntax
		    // localStorage.setItem('propertyName', value);
			localStorage.setItem('token', data.access);
			retrieveUserDetails(data.access);

			setUser({
				access: localStorage.getItem('token')
			})

			Swal.fire({
        	    title: "Login Successful",
        	    icon: "success",
        	    text: "Successfully Logged in!"
        	});
			
		} else {

			Swal.fire({
                title: "Authentication failed",
                icon: "error",
                text: "Check your login details and try again."
            });
		}
	})
	// Clear input fields after submission
	setEmail('');
	setPassword('');


    }


    const retrieveUserDetails = (token) => {
        
        // The token will be sent as part of the request's header information
        // We put "Bearer" in front of the token to follow implementation standards for JWTs
        fetch('https://shoebox-api.onrender.com/users/details', {
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            // localStorage.setItem('token', data.access);

            // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            });

        })

    };

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    return (
      (user.id !== null && user.id !== user.isAdmin) ? (
        user.isAdmin ? <Navigate to="/AdminDashboard" /> : <Navigate to="/" />
      ) : (
        <Container className="spacing"> 
          <Row className="justify-content-center">
            <Col md={6}>
              <Form className="border p-4 rounded mt-5" onSubmit={(e) => authenticate(e)}>
                <h1 className="my-5 text-center">Login</h1>
                <Form.Group controlId="userEmail">
                  <Form.Control 
                    type="email" 
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                    className="mb-2 custom-form-control custom-non-rounded placeholder-text-size"
                  />
                </Form.Group>

                <Form.Group controlId="password">
                  <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                    className="mb-2 custom-form-control custom-non-rounded placeholder-text-size"
                  />
                </Form.Group>

                <Button className="mt-2 btn-custom custom-non-rounded " variant="dark" type="submit" id="submitBtn">
                  SIGN IN
                </Button>
              </Form>
            </Col>    
          </Row>        
        </Container>
      )
    );
}
