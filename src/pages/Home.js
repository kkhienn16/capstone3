import React, { useEffect, useState } from 'react';
import { Navigate } from 'react-router-dom';

import HomePageCarousel from './HomePageCarousel';
import FeaturedProducts from '../components/FeaturedProducts';
import UserView from '../components/UserView';
import ProductSearch from '../components/ProductSearch';
import Footer from '../components/Footer';


export default function Home() {
  const [searchResults, setSearchResults] = useState([]);
  const [searchPerformed, setSearchPerformed] = useState(false);
  const [products, setProducts] = useState([]);
  const [error, setError] = useState(null); // Add state to handle errors

  const fetchData = () => {
    fetch(`https://capstone2-1.onrender.com/products/all`)
      .then((res) => {
        if (!res.ok) {
          throw new Error('Network response was not ok');
        }
        return res.json();
      })
      .then((data) => {
        console.log(data);
        setProducts(data);
      })
      .catch((error) => {
        setError(error); // Handle network or parsing errors here
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSearch = (results) => {
    setSearchResults(results);
    setSearchPerformed(true);
  };

  return (
    <>
      
      <ProductSearch onSearch={handleSearch} />
      {searchPerformed
       ? (
        <Navigate to="/ProductSearch" />
      ) : (
        <>
          <HomePageCarousel />
          <FeaturedProducts />
          <UserView productsData={products} />
            
        </>
      )}

     
    </>
  );
}
