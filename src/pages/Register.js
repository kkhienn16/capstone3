import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Container, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import App from '../App.css';



export default function Register() {

	const { user } = useContext(UserContext);

		// State hooks to store the values of the input fields
	const [firstName,setFirstName] = useState("");
	const [lastName,setLastName] = useState("");
	const [email,setEmail] = useState("");
	const [mobileNo,setMobileNo] = useState("");
	const [password,setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);
    const navigate = useNavigate();



     // Check if values are successfully binded
	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNo);
	console.log(password);
	console.log(confirmPassword)

	function registerUser(e) {


		e.preventDefault();

		fetch('https://capstone2-1.onrender.com/users/register',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

		//data is the response of the api/server after it's been process as JS object through our res.json() method.
		console.log(data);
		//data will only contain an email property if we can properly save our user.
		if(data){

			setFirstName('');
			setLastName('');
			setEmail('');
			setMobileNo('');
			setPassword('');
			setConfirmPassword('');

			Swal.fire({
			  position: 'top-end',
			  icon: 'success',
			  title: 'Created successfully!',
			  showConfirmButton: false,
			  timer: 1500
			});

      navigate('/login');
						
		}
		 else {
			
			Swal.fire({
			  position: 'top-end',
			  icon: 'error',
			  title: 'Something went wrong!',
			  showConfirmButton: false,
			  timer: 1500
			})
		}



		})

	}

	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !=="" && confirmPassword !=="") && (password === confirmPassword) && (mobileNo.length === 11)){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[firstName,lastName,email,mobileNo,password,confirmPassword])

	return (

		(user.id !== null) ?
			<Navigate to="/products" />
		:
      <Container className="spacing">
      <Row className="justify-content-center">
        <Col md={6} style={{ width: '500px' }}>
          <Form onSubmit={(e) => registerUser(e)} className="mt-5">
            <h3 className="my-3 text-center">CREATE ACCOUNT</h3>
            <hr />

            <Form.Group>
              <Form.Control
             		size="lg"
                type="text"
                placeholder="First Name"
                required
                value={firstName}
                onChange={(e) => { setFirstName(e.target.value) }}
                className="mb-3 placeholder-text-size
                	custom-form-control custom-non-rounded"
              />
            </Form.Group>

            <Form.Group>
              <Form.Control
              	size="lg"
                type="text"
                placeholder="Last Name"
                required
                value={lastName}
                onChange={(e) => { setLastName(e.target.value) }}
                className="mb-3 placeholder-text-size
                	custom-form-control custom-non-rounded"
              />
            </Form.Group>

            <Form.Group>
              <Form.Control
              	size="lg"
                type="email"
                placeholder="Email"
                required
                value={email}
                onChange={(e) => { setEmail(e.target.value) }}
                className="mb-3 placeholder-text-size
                	custom-form-control custom-non-rounded"
              />
            </Form.Group>

            <Form.Group>
              <Form.Control
              	size="lg"
                type="number"
                placeholder="11 Digit Mobile No."
                required
                value={mobileNo}
                onChange={(e) => { setMobileNo(e.target.value) }}
                className="mb-3 placeholder-text-size
                	custom-form-control custom-non-rounded"
              />
            </Form.Group>

            <Form.Group>
              <Form.Control
              	size="lg"
                type="password"
                placeholder="Password"
                required
                value={password}
                onChange={(e) => { setPassword(e.target.value) }}
                className="mb-3 placeholder-text-size
                	custom-form-control custom-non-rounded"
              />
            
            </Form.Group>

            <Form.Group>
              <Form.Control
              	size="lg"
                type="password"
                placeholder="Confirm Password"
                required
                value={confirmPassword}
                onChange={(e) => { setConfirmPassword(e.target.value) }}
                className="mb-3 placeholder-text-size
                	custom-form-control custom-non-rounded"
              />
            </Form.Group>
            
               <Button className="mt-3 btn-custom custom-non-rounded" variant="dark" type="submit">CREATE</Button>
       
            
          </Form>
        </Col>
      </Row>
    </Container>
  );


}


