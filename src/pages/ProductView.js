import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Image from 'react-bootstrap/Image';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import RelatedProducts from '../components/RelatedProducts';
import App from '../App';
import Footer from '../components/Footer';


const getRandomImagePath = () => {
  const imagePaths = [
    '../nike90.png', 
    '../j1.png',
    '../j2.png',
    '../j3.png',
    '../j4.png',
    '../j5.png',
    '../j6.png',
    '../j7.png',
    '../j8.png',
    '../j9.png',
    '../j10.png',
    '../j11.png',
    '../j12.png',
    '../j13.png'
  ];

  const randomIndex = Math.floor(Math.random() * imagePaths.length);
  return imagePaths[randomIndex];
};

export default function ProductView({updateTotalQuantity}) {
  const { user } = useContext(UserContext);
  const randomImagePath = getRandomImagePath();
  const navigate = useNavigate();

  // The "useParams" hook allows us to retrieve the productId passed via the URL
  const { productId } = useParams();

  const [brand, setBrand] = useState("");
  const [model, setModel] = useState("");
  const [description, setDescription] = useState("");
  const [size, setSize] = useState("");
  const [availableQuantity, setAvailableQuantity] = useState(0);
  const [price, setPrice] = useState(0);
  const [selectedQuantity, setSelectedQuantity] = useState(1); // Default to 1

  const checkout = () => {
    fetch(`https://capstone2-1.onrender.com/users/checkout`, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        
        productId: productId,
        quantity: selectedQuantity // Send the selected quantity to the server
      })
      
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        if (data.message === 'Purchase Successfully') {
          Swal.fire({
            title: "Checkout successfully",
            icon: 'success',
            text: "You have successfully checked out"
          });
        }

        if (data.message === 'Insufficient stock'){
        Swal.fire({
          title: "Insufficient stock",
          icon: 'error',
          text: "Product not available"
        })
        

      }

         else {
          Swal.fire({
            title: "Something went wrong",
            icon: 'error',
            text: "Please try again."
          });
        }
      })
      .catch(error => {
        console.error('Error during checkout:', error);
        // Handle the error, e.g., show an error message to the user.
      });
  }

  const addToCart = (productId) => {

    fetch(`https://capstone2-1.onrender.com/users/cart/${productId}`, {

      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${ localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        quantity: selectedQuantity
      })

    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if (data.message = 'Add to cart Successfully'){
        Swal.fire({
          title: "Success",
          icon: 'success',
          text: "Item Added to Cart"
        })
        

      } else{
        Swal.fire({
          title: "Something went wrong",
          icon: 'error',
          text: "Please try again."
        })
      }

    })
  }

  useEffect(() => {
    fetch(`https://capstone2-1.onrender.com/products/${productId}`)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        setBrand(data.brand);
        setModel(data.model);
        setDescription(data.description);
        setSize(data.size);
        setAvailableQuantity(data.quantity); // Store the available quantity
        setPrice(data.price);
      })
      .catch(error => {
        console.error('Error fetching product:', error);
        // Handle the error, e.g., show an error message to the user.
      });
  }, [productId]);

  const handleQuantityChange = (event) => {
    const newQuantity = parseInt(event.target.value);
    setSelectedQuantity(newQuantity);
  };

  return (
    <>

    <Container className="mt-5 ">
      <Row>
        <Col sm={12} md={8}>
          <Card.Title className="preview-text">{brand}</Card.Title>
          <Card.Subtitle className="text-muted">{model}</Card.Subtitle>
          <Image src={randomImagePath} className="image-sn" />
        </Col>


        <Col sm={12} md={4}>
          <Card className="text-center  card-check w-100" style={{ boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2)" }}>
            <Card.Body>
              <Card.Subtitle className="m-3 preview-sub-text">SIZE: {size}</Card.Subtitle>
              <Card.Subtitle className="m-3 preview-sub-text">PRICE: ${price} </Card.Subtitle>

              <Form.Group className="p-3 preview-sub-text">
                <Form.Label>QUANTITY</Form.Label>
                <Form.Control
                  type="number"
                  value={selectedQuantity}
                  onChange={handleQuantityChange}
                  min={1}
                  max={availableQuantity}
                />
              </Form.Group>

              {user.id !== null ? (
                <>
                  <Button
                    variant="light"
                    className="m-3 custom-non-rounded mx-3 product-view-btn"
                    block
                    onClick={() => addToCart(productId)}
                  >
                    ADD TO CART
                  </Button>

                  <Button
                    className="m-3 custom-non-rounded product-view-btn home-btn"
                    block
                    onClick={() => checkout(`${productId}`)}
                  >
                    Buy for ${price * selectedQuantity}
                  </Button>

                </>
              ) : (
                <Link className="btn btn-danger btn-block" to="/login">
                  Log in to Checkout
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>

    <Container className="mb-5">
      <hr / >
      <RelatedProducts/>
    </Container>

 
    </>    
  );
}
