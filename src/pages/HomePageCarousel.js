import React from 'react';
import { Carousel } from 'react-bootstrap';
import App from '../App.css'; 
import { Container } from 'react-bootstrap';


const HomePageCarousel = () => {
  return (
    <Container className="mt-3">
  
      <div className="carousel-container" style={{ boxShadow: "2 4px 8px 1 rgba(2, 0, 1, 0.3)" }}>
        <Carousel className="mt-1 ml-5 " >
          <Carousel.Item className="carousel-item d-flex">
            <img
              className="d-block w-100 carousel-image"
              src="../nike.jpg"
              alt="First slide"
            />
           
          </Carousel.Item >
          <Carousel.Item className="carousel-item">
            <img
              className="d-block w-100 carousel-image"
              src="../jordan.jpg"
              alt="Second slide"
            />
          
          </Carousel.Item>
          <Carousel.Item className="carousel-item">
            <img
              className="d-block w-100 carousel-image"
              src="../adidas.jpg"
              alt="Third slide"
            />
          
          </Carousel.Item>
        </Carousel>
      </div>
    </Container>
  );
};

export default HomePageCarousel;
