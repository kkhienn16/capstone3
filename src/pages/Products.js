import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';

export default function Products() {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);
  const [searchResults, setSearchResults] = useState([]);
  const [searchPerformed, setSearchPerformed] = useState(false);

  const fetchData = () => {
    fetch(`https://capstone2-1.onrender.com/products/all`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProducts(data);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSearch = (results) => {
    setSearchResults(results);
    setSearchPerformed(true);
  };

  return (
    <>
      {user.isAdmin ? (
        <AdminView productsData={products} fetchData={fetchData} />
      ) : (
        <UserView
          productsData={searchPerformed ? searchResults : products}
          onSearch={handleSearch}
        />
      )}
    </>
  );
}
