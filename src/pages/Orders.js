import { useContext, useState, useEffect } from 'react';
import { Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import { Link } from 'react-router-dom';

export default function Cart() {
  const { user } = useContext(UserContext);
  const [orderedProducts, setOrderedProducts] = useState([]);
  const [ details, setDetails ] =useState({})

  useEffect(() => {
    fetch(`https://capstone2-1.onrender.com/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if(data !== null && data !== undefined){
            setDetails(data);
        }

        if (Array.isArray(data.orderedProducts)) {
          setOrderedProducts(data.orderedProducts);
         
        }
        console.log(data.orderedProducts);
      });
  }, []);

  return (
    <>
      {!user && <Navigate to="/" />}
      {user && (
        <>
          <Row className="m-3">
            <Col sm={3}>
              <h4 className="mt-5 mb-4">{`${details.firstName} ${details.lastName}`}</h4>

              <div className="d-flex align-items-center mb-3">
        <a href="/cart" className="text-decoration-none d-flex align-items-center a-link">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="22"
            height="30"
            fill="currentColor"
            className="bi bi-cart"
            viewBox="0 0 16 16"
          >
            <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
          </svg>
          <div className="d-flex flex-column ml-2">
            <h6 className="mx-3 mb-0">Cart</h6>
            <p className="mx-3 text-muted mb-0 p-link">Manage your cart</p>
          </div>
        </a>
      </div>

      <div className="d-flex align-items-center mb-3">
        <a href="/orders" className="text-decoration-none d-flex align-items-center a-link">
        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="30" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16"> <path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"/> <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"/> <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/> 
        </svg>

          <div className="d-flex flex-column ml-2">
            <h6 className="mx-3 mb-0">Orders</h6>
            <p className="mx-3 text-muted mb-0 p-link">View order History</p>
          </div>
        </a>
      </div>


          <div className="d-flex align-items-center mb-3">
            <a href="/profile" className="text-decoration-none d-flex align-items-center a-link">
              <svg width="25" height="30" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M24 42C33.9411 42 42 33.9411 42 24C42 14.0589 33.9411 6 24 6C14.0589 6 6 14.0589 6 24C6 33.9411 14.0589 42 24 42ZM24 44C35.0457 44 44 35.0457 44 24C44 12.9543 35.0457 4 24 4C12.9543 4 4 12.9543 4 24C4 35.0457 12.9543 44 24 44Z" fill="#333333"/> <path d="M12 35.6309C12 34.5972 12.772 33.7241 13.7995 33.6103C21.515 32.7559 26.5206 32.8325 34.218 33.6287C35.2324 33.7337 36 34.5918 36 35.6116C36 36.1807 35.7551 36.7275 35.3262 37.1014C26.2414 45.0195 21.0488 44.9103 12.6402 37.1087C12.2306 36.7286 12 36.1897 12 35.6309Z" fill="#333333"/> <path fill-rule="evenodd" clip-rule="evenodd" d="M34.1151 34.6234C26.4784 33.8334 21.5449 33.7587 13.9095 34.6042C13.3954 34.6612 13 35.1002 13 35.6309C13 35.9171 13.1187 36.1885 13.3204 36.3757C17.4879 40.2423 20.6461 41.9887 23.7333 41.9999C26.8309 42.0113 30.1592 40.2783 34.6691 36.3476C34.8767 36.1667 35 35.8964 35 35.6116C35 35.0998 34.6154 34.6752 34.1151 34.6234ZM13.6894 32.6164C21.4852 31.7531 26.5628 31.8315 34.3209 32.6341C35.8495 32.7922 37 34.0838 37 35.6116C37 36.465 36.6336 37.2884 35.9832 37.8553C31.4083 41.8426 27.598 44.0141 23.726 43.9999C19.8435 43.9857 16.2011 41.7767 11.9601 37.8418C11.3425 37.2688 11 36.4624 11 35.6309C11 34.0943 12.1487 32.787 13.6894 32.6164Z" fill="#333333"/> <path d="M32 20C32 24.4183 28.4183 28 24 28C19.5817 28 16 24.4183 16 20C16 15.5817 19.5817 12 24 12C28.4183 12 32 15.5817 32 20Z" fill="#333333"/> <path fill-rule="evenodd" clip-rule="evenodd" d="M24 26C27.3137 26 30 23.3137 30 20C30 16.6863 27.3137 14 24 14C20.6863 14 18 16.6863 18 20C18 23.3137 20.6863 26 24 26ZM24 28C28.4183 28 32 24.4183 32 20C32 15.5817 28.4183 12 24 12C19.5817 12 16 15.5817 16 20C16 24.4183 19.5817 28 24 28Z" fill="#333333"/> </svg>
              <div className="d-flex flex-column ml-2">
                <h6 className="mx-3 mb-0">Profile</h6>
                <p className="mx-3 text-muted mb-0 p-link">View profile</p>
              </div>
            </a>
          </div>

             

              
            </Col>

            <Col sm={6} className="mt-5 mb-5 justify-content-center spacing" >
                <div className="d-flex justify-content-between align-items-center">
                  <h4>ORDER HISTORY</h4>
                 
                </div>

                <hr />

                {orderedProducts.length === 0 ? (
                  <p>No order History.</p>
                ) : (
                  <div className="mt-3">
                    <div className="table-responsive">
                      <table className="table table-striped table-bordered table-hover table-light">
                        <thead>
                          <tr className="text-center">
                            <th>Purchased Date</th>
                            <th>Model</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                          </tr>
                        </thead>
                        <tbody>
                          {orderedProducts.map((item, index) => (
                            <tr key={index} className="text-center">
                              <td>{item.purchasedOn}</td>
                              <td>{item.products[0].productModel}</td>
                              <td>${item.products[0].price}</td>
                              <td>{item.products[0].quantity}</td>
                             
                              <td>${item.subTotal}</td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                )}
              </Col>
          </Row>
        </>
      )}
    </>
  );
}
