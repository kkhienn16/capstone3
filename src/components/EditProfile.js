import React, { useState } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

const EditProfile = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [message, setMessage] = useState('');
  const [isSuccess, setIsSuccess] = useState(false); 

  const handleEditProfile = async (e) => {
    e.preventDefault();

    const token = localStorage.getItem('token');

    try {
      const response = await fetch('https://capstone2-1.onrender.com/users/profile/', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({ firstName, lastName, mobileNo }),
      });

      if (response.ok) {
        Swal.fire({
          title: 'Successful',
          icon: 'success',
          text: 'Profile updated successfully!',
        });

        setIsSuccess(true);

      } else {
        Swal.fire({
          title: 'Something went wrong',
          icon: 'error',
          text: 'Failed to update profile',
        });
      }
    } catch (error) {
      setMessage('An error occurred. Please try again.');
      console.error(error);
    }
  };

  if (isSuccess) {
    // Use Navigate to navigate to the homepage on success
    return <Navigate to="/" />;
  }

  return (
    <Container>
      <Row className="justify-content-center spacing  ">
        <Col md={6}>
          <Form onSubmit={handleEditProfile}>
            <h2 className="my-5 text-center">Edit Profile</h2>
            <hr />
            <Form.Group controlId="firstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                required
                className="mb-2 custom-form-control custom-non-rounded placeholder-text-size"
              />
            </Form.Group>
            <Form.Group controlId="lastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                required
                className="mb-2 custom-form-control custom-non-rounded placeholder-text-size"
              />
            </Form.Group>
            <Form.Group controlId="mobileNo">
              <Form.Label>Mobile Number</Form.Label>
              <Form.Control
                type="text"
                value={mobileNo}
                onChange={(e) => setMobileNo(e.target.value)}
                required
                className="mb-2 custom-form-control custom-non-rounded placeholder-text-size"
              />
            </Form.Group>
            {message && <div className="alert alert-danger">{message}</div>}
            <Button type="submit" variant="dark" className="btn-custom custom-non-rounded">
              Save Changes
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default EditProfile;
