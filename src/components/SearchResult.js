
import React from 'react';
import { Card, Button, Row, Col, Container } from 'react-bootstrap';
import ProductCard from './ProductCard';

export default SearchResults  ({ searchResults }) {
  return (
    <Container>
      
        <div className="d-md-flex">
          {searchResults.map((product) => (
            <ProductCard key={product._id} productProp={product} />
          ))}
        </div>
     
    <Container>  
  );
};

export SearchResults;
