import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import App from '../App.css';

export default function UpdateUser({user, isAdmin, fetchData}) {

	const userToggle = (userId) => {
		fetch(`https://capstone2-1.onrender.com/users/${userId}/updateUser`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Success'
				})
				fetchData();
			} else {
				  Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'Error',
                    text: 'Please Try again'
                })
                fetchData();
			}
		})
	}


	const adminToggle = (userId) => {
		fetch(`https://capstone2-1.onrender.com/users/${userId}/updateAdmin`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully disabled'
				})
				fetchData();
			} else {
				  Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'error',
                    text: 'Please Try again'
                })
                fetchData();
			}
		})
	}

	return (

		<>
			{isAdmin ?

                <Button className="custom-non-rounded" variant="danger" size="sm" onClick={() => userToggle(user)}>Revoke as Admin</Button>

                :

                <Button className="custom-non-rounded" variant="success" size="sm" onClick={() => adminToggle(user)}>Make Admin</Button>

            }
		</>
		)

}	