import React, { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
import App from '../App.css';
import { Container, Row } from 'react-bootstrap';



const ProductSearch = ({ productsData }) => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [searchPerformed, setSearchPerformed] = useState(false);

  // Use useEffect to update searchPerformed when searchResults change
  useEffect(() => {
    setSearchPerformed(searchResults.length > 0);
  }, [searchResults]);

  const handleSearch = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch('https://capstone2-1.onrender.com/products/search', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ productName: searchQuery }),
      });
      const data = await response.json();
      setSearchResults(data);
    } catch (error) {
      console.error('Error searching for products:', error);
    }
  };

  return (
    <>
      <Container>
        <div className="my-auto mt-3">
          <form onSubmit={handleSearch}>
            <div className="search-box">
              <input
                type="text"
                className="search-input"
                placeholder="Search for products..."
                id="productName"
                value={searchQuery}
                onChange={(event) => setSearchQuery(event.target.value)}
              />
              <button className="search-button">
                <span className="material-symbols-outlined">search</span>
              </button>
            </div>
          </form>
        </div>

        <div className="row">
          {productsData === null ? (
            <p>No Product found</p>
          ) : (
            <>
              {searchPerformed && searchResults.length > 0 && (
                <>
                  <h3>Search Result:</h3>
                <Container>  
                  <div className="row">
                    <div className="">
                      <ul className="d-md-flex">
                        {searchResults.map((product) => (
                          <ProductCard productProp={product} key={product._id} />
                        ))}
                      </ul>
                    </div>
                  </div>  
                </Container>  
                </>
              )}
            </>
          )}
        </div>


      </Container>
    </>
  );
};

export default ProductSearch;
