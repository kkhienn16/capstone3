import { useContext, useState, useEffect } from 'react';
import { Table, Button, Nav, Row, Col } from 'react-bootstrap';
import { Link, NavLink, Navigate } from 'react-router-dom';
import EditProduct from './EditProduct';
import UserContext from '../UserContext';
import ArchiveProduct from './ArchiveProduct';

export default function AdminView ({ productsData, fetchData}) {

   const { user } = useContext(UserContext);
	 const [products, setProducts] = useState([]);
    
      useEffect(() => {
        fetch(`https://capstone2-1.onrender.com/users/details`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        })
          .then((res) => res.json())
          .then((data) => {
            console.log(data);

            // if (data !== null && data !== undefined) {
            //   setDetails(data);
            // }
          });
      }, []);

	useEffect(() => {
   // Check if productsData is an array
    const productsArr = productsData.map((product) => {
      console.log(productsData)
      return (
        <tr key={product._id}>
          <td>{product._id}</td>
          <td>{product.brand}</td>
          <td>{product.model}</td>
          <td>{product.description}</td>
          <td>{product.size}</td>
          <td>{product.stock}</td>
          <td>{product.price}</td>
          <td className={product.isActive ? 'text-success' : 'text-danger'}>
            {product.isActive ? 'Available' : 'Unavailable'}
          </td>
          <td><EditProduct product={product._id} fetchData={fetchData}/></td>
          <td><ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData}/></td>
        </tr>
      );
    });

    setProducts(productsArr);
  
}, [productsData]);
    return (
        <>  
            <h1 className="text-center my-4 admin-dashboard"> ADMIN DASHBOARD</h1>
            <hr/>

            <Row className="m-3">

                <Col sm={3}>

                  <div className="d-flex align-items-center mb-3">
                  <Nav.Link  as={Link} to="/add-product" className="text-decoration-none d-flex align-items-center a-link">
                 <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bag-check" viewBox="0 0 16 16"> 
                 <path fill-rule="evenodd" d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z"/> <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z"/> 
                 </svg>
                    <div className="d-flex flex-column ml-2">
                      <h6 className="mx-3 mb-0">Add Product</h6>
                      <p className="mx-3 text-muted mb-0 p-link">Add additional products</p>
                    </div>
                    </Nav.Link>
                </div>  


                <div className="d-flex align-items-center mb-3">
                  <Nav.Link  as={Link} to="/products" className="text-decoration-none d-flex align-items-center a-link">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-card-checklist" viewBox="0 0 16 16"> <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/> <path d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0zM7 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z"/> </svg>
                    <div className="d-flex flex-column ml-2">
                      <h6 className="mx-3 mb-0">Products</h6>
                      <p className="mx-3 text-muted mb-0 p-link">Edit, Archive or Activate Products</p>
                    </div>
                    </Nav.Link>
                </div>  


                <div className="d-flex align-items-center mb-3">
                  <Nav.Link  as={Link} to="/AllOrder" className="text-decoration-none d-flex align-items-center a-link">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-seam" viewBox="0 0 16 16"> <path d="M8.186 1.113a.5.5 0 0 0-.372 0L1.846 3.5l2.404.961L10.404 2l-2.218-.887zm3.564 1.426L5.596 5 8 5.961 14.154 3.5l-2.404-.961zm3.25 1.7-6.5 2.6v7.922l6.5-2.6V4.24zM7.5 14.762V6.838L1 4.239v7.923l6.5 2.6zM7.443.184a1.5 1.5 0 0 1 1.114 0l7.129 2.852A.5.5 0 0 1 16 3.5v8.662a1 1 0 0 1-.629.928l-7.185 2.874a.5.5 0 0 1-.372 0L.63 13.09a1 1 0 0 1-.63-.928V3.5a.5.5 0 0 1 .314-.464L7.443.184z"/> </svg>

                    <div className="d-flex flex-column ml-2">
                      <h6 className="mx-3 mb-0">Orders</h6>
                      <p className="mx-3 text-muted mb-0 p-link">List of all ordered products</p>
                    </div>
                    </Nav.Link>
                </div>

                <div className="d-flex align-items-center mb-3">
                  <Nav.Link  as={Link} to="/UsersList" className="text-decoration-none d-flex align-items-center a-link">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-card-list" viewBox="0 0 16 16"> <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/> <path d="M5 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 5 8zm0-2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0 5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-1-5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zM4 8a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zm0 2.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z"/> </svg>

                    <div className="d-flex flex-column ml-2">
                      <h6 className="mx-3 mb-0">Users List</h6>
                      <p className="mx-3 text-muted mb-0 p-link">List of all verified users</p>
                    </div>
                    </Nav.Link>
                </div> 

                   <div className="d-flex align-items-center mb-3">
                    <Nav.Link  as={Link} to="/AdminDashboard" className="text-decoration-none d-flex align-items-center a-link">
                    <svg width="25" height="30" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M24 42C33.9411 42 42 33.9411 42 24C42 14.0589 33.9411 6 24 6C14.0589 6 6 14.0589 6 24C6 33.9411 14.0589 42 24 42ZM24 44C35.0457 44 44 35.0457 44 24C44 12.9543 35.0457 4 24 4C12.9543 4 4 12.9543 4 24C4 35.0457 12.9543 44 24 44Z" fill="#333333"/> <path d="M12 35.6309C12 34.5972 12.772 33.7241 13.7995 33.6103C21.515 32.7559 26.5206 32.8325 34.218 33.6287C35.2324 33.7337 36 34.5918 36 35.6116C36 36.1807 35.7551 36.7275 35.3262 37.1014C26.2414 45.0195 21.0488 44.9103 12.6402 37.1087C12.2306 36.7286 12 36.1897 12 35.6309Z" fill="#333333"/> <path fill-rule="evenodd" clip-rule="evenodd" d="M34.1151 34.6234C26.4784 33.8334 21.5449 33.7587 13.9095 34.6042C13.3954 34.6612 13 35.1002 13 35.6309C13 35.9171 13.1187 36.1885 13.3204 36.3757C17.4879 40.2423 20.6461 41.9887 23.7333 41.9999C26.8309 42.0113 30.1592 40.2783 34.6691 36.3476C34.8767 36.1667 35 35.8964 35 35.6116C35 35.0998 34.6154 34.6752 34.1151 34.6234ZM13.6894 32.6164C21.4852 31.7531 26.5628 31.8315 34.3209 32.6341C35.8495 32.7922 37 34.0838 37 35.6116C37 36.465 36.6336 37.2884 35.9832 37.8553C31.4083 41.8426 27.598 44.0141 23.726 43.9999C19.8435 43.9857 16.2011 41.7767 11.9601 37.8418C11.3425 37.2688 11 36.4624 11 35.6309C11 34.0943 12.1487 32.787 13.6894 32.6164Z" fill="#333333"/> <path d="M32 20C32 24.4183 28.4183 28 24 28C19.5817 28 16 24.4183 16 20C16 15.5817 19.5817 12 24 12C28.4183 12 32 15.5817 32 20Z" fill="#333333"/> <path fill-rule="evenodd" clip-rule="evenodd" d="M24 26C27.3137 26 30 23.3137 30 20C30 16.6863 27.3137 14 24 14C20.6863 14 18 16.6863 18 20C18 23.3137 20.6863 26 24 26ZM24 28C28.4183 28 32 24.4183 32 20C32 15.5817 28.4183 12 24 12C19.5817 12 16 15.5817 16 20C16 24.4183 19.5817 28 24 28Z" fill="#333333"/> </svg>

                      <div className="d-flex flex-column ml-2">
                        <h6 className="mx-3 mb-0">Profile</h6>
                        <p className="mx-3 text-muted mb-0 p-link">View and edit profile info.</p>
                      </div>
                      </Nav.Link>
                  </div>   

                     
                </Col>  

                <Col sm={8}>
                    <div lassName="mt-5 justify-content-center">
                        <div className=" mt-3 row">
                            <table className="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr className="text-center">
                                        <th>ID</th>
                                        <th>Brand</th>
                                        <th>Model</th>
                                        <th>Description</th>
                                        <th>Size</th>
                                        <th>Stock</th>
                                        <th>Price</th>
                                        <th>Availability</th>
                                        <th colSpan="2">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {products}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </Col>    
            </Row>    
        </>
    );
}