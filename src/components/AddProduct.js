import React, { useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Container, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddProduct() {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);

  // Input states
  const [brand, setBrand] = useState('');
  const [model, setModel] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [stock, setStock] = useState(0);
  const [size, setSize] = useState('');
  const [productImage, setProductImage] = useState(null); // New state for image upload

  async function createProduct(e) {
    e.preventDefault();

    try {
      let token = localStorage.getItem('token');

      // Create a FormData object to send both text and image data
      const formData = new FormData();
      formData.append('brand', brand);
      formData.append('model', model);
      formData.append('description', description);
      formData.append('price', price);
      formData.append('stock', stock);
      formData.append('size', size);
      formData.append('productImage', productImage); // Append the image

      const response = await fetch('https://capstone2-1.onrender.com/products/', {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData, // Send the FormData object
      });

      const data = await response.json();

      if (response.ok) {
        console.log(data);
        Swal.fire({
          icon: 'success',
          title: 'Product Added',
        });
        navigate('/products');
      } else {
        console.error(data);
        Swal.fire({
          icon: 'error',
          title: 'Unsuccessful Product Creation',
          text: 'Failed to add the product.',
        });
      }
    } catch (error) {
      console.error('Error:', error);
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'An error occurred while adding the product.',
      });
    }

    // Clear form fields after submission
    setBrand('');
    setModel('');
    setDescription('');
    setPrice('');
    setStock(0);
    setSize('');
    setProductImage(null); // Clear the image input
  }

  return user.isAdmin === true ? (
    <>
      <Container className="spacing">
        <Row className="justify-content-center">
          <Col md={6} style={{ width: '500px' }}>
            <Form onSubmit={createProduct}>
              <h1 className="my-3 text-center">ADD PRODUCT</h1>
              <hr />

               <Form.Group>
                <Form.Label>Brand:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Brand"
                  required
                  value={brand}
                  onChange={(e) => setBrand(e.target.value)}
                  className="mb-3 placeholder-text-size
                  custom-form-control custom-non-rounded"
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>Model:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Model"
                  required
                  value={model}
                  onChange={(e) => setModel(e.target.value)}
                   className="mb-3 placeholder-text-size
                  custom-form-control custom-non-rounded"
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>Description:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Description"
                  required
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                   className="mb-3 placeholder-text-size
                  custom-form-control custom-non-rounded"
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>Price:</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Enter Price"
                  required
                  value={price}
                  onChange={(e) => setPrice(e.target.value)}
                   className="mb-3 placeholder-text-size
                  custom-form-control custom-non-rounded"
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>Stock:</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Enter Stock"
                  required
                  value={stock}
                  onChange={(e) => setStock(e.target.value)}
                   className="mb-3 placeholder-text-size
                  custom-form-control custom-non-rounded"
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>Size:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Size"
                  required
                  value={size}
                  onChange={(e) => setSize(e.target.value)}
                   className="mb-3 placeholder-text-size
                  custom-form-control custom-non-rounded"
                />
              </Form.Group>


              {/* Input field for image upload */}
              <Form.Group>
                <Form.Label>Product Image:</Form.Label>
                <Form.Control
                  type="file"
                  accept="image/*"
                  onChange={(e) => setProductImage(e.target.files[0])}
                />
              </Form.Group>

              {/* Rest of the form inputs (brand, model, description, etc.) remain the same as before */}

              <Button variant="dark" type="submit" className="mt-3 btn-custom custom-non-rounded">
                SUBMIT
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    </>
  ) : (
    <Navigate to="/products" />
  );
}
