import React, { useState } from "react";

const RemoveCartItem = ({ productId, onRemove }) => {
  const [isLoading, setIsLoading] = useState(false);

  const handleRemove = () => {
    setIsLoading(true);
    fetch(`https://capstone2-1.onrender.com/users/cart/delete/${productId}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`, 
      },
    })
      .then((response) => {
        if (response.ok) {
          onRemove(productId); // Call the parent component's callback to update the cart
        } else {
          console.error("Error removing cart item.");
        }
      })
      .catch((error) => {
        console.error("Network error:", error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <>
    <button
      className="btn btn-danger custom-non-rounded "
      onClick={handleRemove}
      disabled={isLoading}
    >
      {isLoading ? "Removing..." : "Remove"}
    </button>

    </>
  );
};

export default RemoveCartItem;
