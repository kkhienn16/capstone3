import { useState, useEffect } from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PreviewProducts from './PreviewProducts';
import '../App.css'

export default function FeaturedProducts(){

	const [previews, setPreviews] = useState([]);

	  useEffect(() => {
	    fetch(`https://capstone2-1.onrender.com/products/`)
	      .then((res) => res.json())
	      .then((data) => {
	        const featured = [];

	        // Shuffle the data array randomly
	        const shuffledData = [...data].sort(() => Math.random() - 0.5);

	        // Get the first 5 items from the shuffled array
	        const featuredData = shuffledData.slice(0, 4);

	        featuredData.forEach((item) => {
	          featured.push(
	            <PreviewProducts data={item} key={item._id} breakPoint={2} />
	          );
	        });

	        setPreviews(featured);
	      });
	  }, []);
	return(
		<>

			
			<Container className="mt-2">
			  <h4 className="text-style">Recommended For You</h4>
			  
			  <div className="row">
			   
			      {previews}
			   </div>
			 
			</Container>
		</>
	)


}