import React from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';

const Footer = () => {
  return (
    <footer className="bg-dark text-light py-4 mt-5 ">
      <Container fluid >
        <Row className="d-flex justify-content-center">

          <Col xs={12} md={2} className="footer-margin mt-3">
            <h4>Brands</h4>
            <a href="https://www.nike.com/"target="_blank" className="footer-links">
              <p>Nike</p>
            </a>

            <a href="https://www.adidas.com.ph/"target="_blank" className="footer-links">
              <p>Adidas</p>
            </a>

            <a href="https://www.nike.com/"target="_blank" className="footer-links">
              <p> Rebook </p>
            </a>

            <a href="https://www.newbalance.com/"target="_blank" className="footer-links">New Balance</a>
          
          
          </Col>

          <Col xs={12} md={2} className="footer-margin mt-3">
            <h4>Social Media</h4>
            <a href="https://www.facebook.com/"target="_blank" className="footer-links">
              <p>Facebook</p>
            </a>

            <a href="https://www.twitter.com/"target="_blank " className="footer-links">
              <p>Twitter</p>
            </a>

            <a href="https://www.instagram.com/"target="_blank" className="footer-links">
              <p>Instagram</p>
            </a>

            <a href="https://www.youtube.com/"target="_blank" className="footer-links">Youtube</a>
            
          </Col>

          <Col xs={12} md={2} className="footer-margin mt-3" >
            <h4>Contact Us</h4>
            <address>
              <p>Fortich Street, Kalasungay</p>
              <p>Malaybalay, 8700</p>
              <p>Email: khien.bustillo16@gmail.com</p>
              <p>Phone: +63 (977) 869-747</p>
            </address>
          </Col>

           <Col xs={12} md={3} className="footer-margin mt-3" >
            <h4>News Letter</h4>
                <Form>
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                    
                    <Form.Control type="text"
                     placeholder="Your Name"
                     required
                     className="w-50 footer-form"
                      />

                  </Form.Group>
                  <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                    <Form.Control type="email"
                     placeholder="Enter your email"
                     required
                      required
                     className="w-50 footer-form"
                      />
                  </Form.Group>
                  <Button className="footer-btn"> Subscribe</Button>
                </Form>

            </Col>


        </Row>

          
      </Container>
      <Container fluid>
        <hr/>
           <div class="row mt-5">

                <div class="col mx-5">
                  <p>&#169; 2023 SHOEBOX INC. All rights Reserved</p>
                  
                </div>

                <div class="col text-end mx-5">
                  <p>Designed and Developed by: KHIEN BUSTILLO</p>
                </div>
          </div>
      </Container>    
    </footer>
  );
};

export default Footer;
