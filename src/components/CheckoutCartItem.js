import React, { useState } from "react";
import Swal from 'sweetalert2';

const CheckoutCartItem = ({ productId, onRemove }) => {
  const [isLoading, setIsLoading] = useState(false);

  const handleRemove = () => {
    setIsLoading(true);
    fetch(`https://capstone2-1.onrender.com/users/cart/checkout/${productId}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`, 
      },
    })
      .then((response) => {
        if (response.ok) {
         
          onRemove(productId); 
          Swal.fire({
          title: 'Success',
          icon: 'success',
          text: 'Successfully checkout'
          })

        } else {
          console.error("Error checkout cart item.");
          console.log(response)
          Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'error',
                    text: 'Please Try again'
                })
        }
      })
      .catch((error) => {
        console.error("Network error:", error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <>
    <button
      className="btn btn-primary custom-non-rounded"
      onClick={handleRemove}
      disabled={isLoading}
    >
      {isLoading ? "Removing..." : "Checkout"}
    </button>

    </>
  );
};

export default CheckoutCartItem;

