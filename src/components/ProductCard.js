import React from 'react';
import { Card } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';



export default function ProductCard({ productProp }) {
  const { _id, brand, model, description, price, size } = productProp;
  const randomImagePath = getRandomImagePath();

  return (
    <Card className="mx-1 mt-2 h-100" style={{ boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2)" }}>
      <Card.Body className="d-md-flex flex-column">
        <Card.Title>
          {brand}, {model}
        </Card.Title>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text className="card-styles">${price}</Card.Text>
        <Link className="btn home-btn card-btn" to={`/products/${_id}`}>
          Details
        </Link>
      </Card.Body>
    </Card>
  );
}

ProductCard.propTypes = {
  productProp: PropTypes.shape({
    brand: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
    size: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};
