import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Navigate } from 'react-router-dom';

const ResetPassword = () => {
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [message, setMessage] = useState('');
  const [isSuccess, setIsSuccess] = useState(false);

  const handleResetPassword = async (e) => {
    e.preventDefault();

    if (password !== confirmPassword) {
      setMessage('Passwords do not match');
      return;
    }

    try {
      const token = localStorage.getItem('token');
      const response = await fetch('https://capstone2-1.onrender.com/users/reset-password', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          currentPassword: password,
          newPassword: password,
        }),
      });

      if (response.ok) {
        Swal.fire({
          title: 'Successful',
          icon: 'success',
          text: 'Change password Successfully!',
        });
        setIsSuccess(true); 
        
      } else {
        Swal.fire({
          title: 'Something went wrong',
          icon: 'error',
          text: 'Invalid credentials',
        });
      }
    } catch (error) {
      setMessage('An error occurred. Please try again.');
      console.error(error);
    }
  };

  if (isSuccess) {
    return <Navigate to="/" />;
  }

  return (
    <Container>
      <Row className="justify-content-center spacing">
        <Col md={6}>
          <form onSubmit={handleResetPassword}>
            <h2 className="my-5 text-center">RESET PASSWORD</h2>
            <hr />

            <div className="mb-3">
              <label htmlFor="password" className="form-label">
                New Password
              </label>
              <input
                type="password"
                className="form-control"
                id="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
                className="mb-2 custom-form-control custom-non-rounded placeholder-text-size"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="confirmPassword" className="form-label">
                Confirm Password
              </label>
              <input
                type="password"
                className="form-control"
                id="confirmPassword"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
                required
                className="mb-2 custom-form-control custom-non-rounded placeholder-text-size"
              />
            </div>
            {message && <div className="alert alert-danger">{message}</div>}
            <button
              type="submit"
              className="btn btn-dark btn-custom custom-non-rounded"
              variant="dark"
            >
              Reset Password
            </button>
          </form>
        </Col>
      </Row>
    </Container>
  );
};

export default ResetPassword;
