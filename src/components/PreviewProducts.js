import React from 'react';
import { Col, Row, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ProductCard from './ProductCard';

export default function Product(props) {
  const { breakPoint, data } = props;

  const { _id } = data;

  return (

   
    <Col xs={12} sm={6} md={4} lg={3}>
      {/* Use the ProductCard component */}
        <div className="d-md-flex">
           <ProductCard productProp={data} />
        </div>
  
    </Col>
   
  );
}
