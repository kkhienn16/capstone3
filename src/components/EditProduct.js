import React, { useState, useEffect } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import '../App.css'


export default function EditProduct({product, fetchData}) {

	// state fpr productId for the fetch URL
	const [productId, setProductId] = useState('');

	//Forms state
	//Add state for the forms of product
	const [brand, setBrand] = useState('');
	const [model, setModel] = useState('');
	const [description, setDescription] = useState('');
	const [size, setSize] = useState('');
	const [stock, setStock] = useState(0);
	const [price, setPrice] = useState(0);

	const [showEdit, setShowEdit] = useState(false);

	  // Use useEffect to set productId when component mounts
	  useEffect(() => {
	    if (product && product._id) {
	      setProductId(product._id);
	    }
	  }, [product]);





	const openEdit = (productId) => {

		fetch(`https://capstone2-1.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log('productId', productId)
			console.log("API Response:", data);

			setProductId(productId);
			setBrand(data.brand);
			setModel(data.model);
			setDescription(data.description);
			setSize(data.size);
			setStock(data.stock);
			setPrice(data.price);

		})

		setShowEdit(true);
		}

	const closeEdit = () => {

		setShowEdit(false);
		setBrand('');
		setModel('');
		setDescription('');
		setSize('');
		setStock(0);
		setPrice(0);
	}

	const editProduct = (e, productId) => {
		console.log('productId:', productId);
		e.preventDefault();
		 

		fetch(`https://capstone2-1.onrender.com/products/${productId}`,{

			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				brand: brand,
				model: model,
				description: description,
				size: size,
				stock: stock,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title:'Success!',
					icon:'success',
					text:'Product Successfully Updated'
				})
				closeEdit();
				fetchData();

			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
				closeEdit();
				fetchData();

			}

		})
	}

	return(

		<>
			<Button variant="warning" className="custom-non-rounded" size="sm" onClick={() =>{
				console.log("Product:", product);
				openEdit(product);
				}}>Edit
			</Button>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e=>editProduct(e, productId)}>

					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>

						<Form.Group>
							<Form.Label>Brand</Form.Label>
							<Form.Control
								type="text"
								value={brand}
								onChange={e=>setBrand(e.target.value)}
								required
								className="mb-3 placeholder-text-size
                				custom-form-control custom-non-rounded"
								/>

						</Form.Group>

						<Form.Group>
							<Form.Label>Model</Form.Label>
							<Form.Control
								type="text"
								value={model}
								onChange={e=>setModel(e.target.value)}
								required
								className="mb-3 placeholder-text-size
                				custom-form-control custom-non-rounded"
								/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control
								type="text"
								value={description}
								onChange={e=>setDescription(e.target.value)}
								required
								className="mb-3 placeholder-text-size
                				custom-form-control custom-non-rounded"
								/>
						</Form.Group>

						<Form.Group >
							<Form.Label>Size</Form.Label>
							<Form.Control
								type="text"
								value={size}
								onChange={e=>setSize(e.target.value)}
								required
								className="mb-3 placeholder-text-size
                				custom-form-control custom-non-rounded"
								/>
						</Form.Group>

						<Form.Group >
							<Form.Label>Stock</Form.Label>
							<Form.Control
								type="text"
								value={stock}
								onChange={e=>setStock(e.target.value)}
								required
								className="mb-3 placeholder-text-size
                				custom-form-control custom-non-rounded"
								/>
						</Form.Group>

						<Form.Group >
							<Form.Label>Price</Form.Label>
							<Form.Control
								type="text"
								value={price}
								onChange={e=>setPrice(e.target.value)}
								required
								className="mb-3 placeholder-text-size
                				custom-form-control custom-non-rounded"
								/>
						</Form.Group>


					</Modal.Body>

					<Modal.Footer>
						<Button variant="danger" className="custom-non-rounded" onClick={closeEdit}>Close</Button>
						<Button variant="success" className="custom-non-rounded" type="submit">Submit</Button>
					</Modal.Footer>

				</Form>


			</Modal>

		</>
		)

}