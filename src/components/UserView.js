import { useState, useEffect } from 'react';
import React from 'react';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import ProductCard from './ProductCard';
import ProductSearch from './ProductSearch';
import { Card, Button, Row, Col, Container } from 'react-bootstrap';

export default function UserView({ productsData }) {
  const [products, setProducts] = useState([]);
  const [searchResults, setSearchResults] = useState([]);
  const [searchPerformed, setSearchPerformed] = useState(false);

  useEffect(() => {
    const productsArr = productsData.map((product) => {
      if (product.isActive === true) {
        return <ProductCard productProp={product} key={product._id} />;
      } else {
        return null;
      }
    });

    setProducts(productsArr);
  }, [productsData]);

  const responsive = {
      superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 5
      },
      desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3
      },
      tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2
      },
      mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1
      }
    };
   



  return (
    
        <> 
        <Container className="mt-5"> 
          <h3 className="text-style">Popular Brands</h3> 
          <Carousel responsive={responsive} style={{ boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.3)' }}>

            <div className="cardss">
              <img className="product--image" 
                src="../nike1.png"/>
             
            </div>

            <div className="cardss">
              <img className="product--image" src="../adidas1.png"/>
              
           
            </div>

            <div className="cardss">
              <img className="product--image" src="../supreme1.png"/>
            
         
            </div>

            <div className="cardss">
              <img className="product--image" src="../vuiton.png"/>
              
    
            </div>

             <div className="cardss">
              <img className="product--image" src="../jordan1.png"/>
             
           
            </div>
           
          </Carousel>
        </Container>   


         <Container className="mt-5">
           <h3 className="text-style">Hot deals </h3> 
            <div className="row">
              {productsData.length === 0 ? (
                <p>No products found.</p>
              ) : (
                products.map((product, index) => (
                  <div className="col-12 col-sm-6 col-md-4 col-lg-3 mb-4" key={index}>
                    {product}
                  </div>
                ))
              )}
            </div>
          </Container>

      
        </>
    
  );
}
