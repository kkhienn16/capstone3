import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import AddProduct from './components/AddProduct';
import ProductSearch from './components/ProductSearch';
import AdminDashboard from './components/AdminDashboard';
import ResetPassword from './components/ResetPassword';
import EditProfile from './components/EditProfile';
import AllOrder from './components/AllOrder';
import UsersList from './components/UsersList';
import Footer from './components/Footer';




import Profile from './pages/Profile';
import Register from './pages/Register';
import Login from './pages/Login';
import Home from './pages/Home';
import Logout from './pages/Logout';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import { UserProvider } from './UserContext';
import Cart from './pages/Cart';
import Orders from './pages/Orders'
import './App.css';





function App() {


  const [user, setUser] = useState({
    //token: localStorage.getItem('token')
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    //console.log(user);
    //console.log(localStorage);
    fetch(`https://capstone2-1.onrender.com/users/details`,{
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      console.log("App.js")
      // Set the user state values with the user details upon successful login.
      if (typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      else{
        setUser({
          id: null,
          isAdmin: null
        })
      } 
    })
  }, [])
   
  return (
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
              <AppNavbar />
              
          <Container fluid>
          


            <Routes>

              <Route path="/" element={<Home/>} />
              <Route path="/register" element={<Register/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route exact path="/products/:productId" element={<ProductView/>}/>
              <Route path="/logout" element={<Logout />} />
              <Route path="/add-product" element={<AddProduct />} />
              <Route path="/AdminDashboard" element={<AdminDashboard />} />
              <Route path="/products" element={<Products/>} />
               <Route path="/ProductSearch" element={<ProductSearch />} />
              <Route path="/profile" element={<Profile/>} />
              <Route path="/reset-password" element={<ResetPassword/>} />
              <Route path="/update-profile/" element={<EditProfile/>} />
              // <Route path="/cart" element={<Cart/>} />
              <Route path="/cart/:productId" element={<Cart />} />
              <Route path="/orders" element={<Orders/>} />
              <Route path="/allOrder" element={<AllOrder/>} />
              <Route path="/UsersList" element={<UsersList/>} />


            </Routes>
            
          </Container>
           <Footer/>
        </Router>
      </UserProvider>
    );

}

export default App;